# Tom's retail calculator

* Install requirements in current virtualenv
```shell
pip install -r requirements.txt
```

* To run tests
```shell
pytest tests.py
```

* To run app
  * Start backend service
    ```shell
    python main.py
    ```
  * Open `index.html` in browser
