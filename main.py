import decimal

import flask
import pydantic

app = flask.Flask(__name__)

D = decimal.Decimal

_DISCOUNT_BRACKETS = (
    (D('1_000'), D('3')),
    (D('5_000'), D('5')),
    (D('7_000'), D('7')),
    (D('10_000'), D('10')),
    (D('50_000'), D('15')),
)

_TAX_RATES = {
    'UT': D('6.85'),
    'NV': D('8'),
    'TX': D('6.25'),
    'AL': D('4'),
    'CA': D('8.25')
}


def get_discount(total: D) -> D:
    for bracket_limit, discount in reversed(_DISCOUNT_BRACKETS):
        if total >= bracket_limit:
            return discount
    return D(0)


def calc_discounted_total(price: D, amount: int) -> D:
    total = price * amount
    discount = get_discount(total)
    return total * (1 - discount/100)


def calc_taxed_total(total: D, tax_rate: D) -> D:
    return total * (1 + tax_rate/100)


class CalcCostRequest(pydantic.BaseModel):
    price: pydantic.condecimal(ge=D(0), decimal_places=2)
    amount: pydantic.conint(ge=0)
    state: str

    @pydantic.validator('state')
    def validate_state(cls, value: str) -> str:
        if value not in _TAX_RATES:
            raise ValueError(f'Unknown state: {value}')
        return value


class CalcCostResponse(pydantic.BaseModel):
    discounted_total: D
    taxed_total: D

    class Config:
        json_encoders = {
            decimal.Decimal: str,
        }


class ErrorResponse(pydantic.BaseModel):
    code: str
    additional: list


def model_response(model: pydantic.BaseModel, status: int = 200) -> flask.Response:
    return flask.Response(
        model.json(),
        mimetype='application/json',
        status=status,
        headers={'Access-Control-Allow-Origin': '*'},
    )


@app.route('/v1/calc-cost', methods=['POST'])
def calc_cost():
    print(flask.request.get_data())
    try:
        data = CalcCostRequest.parse_raw(flask.request.get_data())
    except pydantic.ValidationError as err:
        result = ErrorResponse(
            code='BAD_REQUEST',
            additional=err.errors(),
        )
        return model_response(result, status=400)

    tax_rate = _TAX_RATES[data.state]

    discounted_total = calc_discounted_total(data.price, data.amount)
    taxed_total = calc_taxed_total(discounted_total, tax_rate)

    result = CalcCostResponse(
        discounted_total=discounted_total.quantize(D('0.01')),
        taxed_total=taxed_total.quantize(D('0.01')),
    )

    return model_response(result)


if __name__ == '__main__':
    app.run('localhost', 5000)
