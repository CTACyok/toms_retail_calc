import flask.testing
import pytest

import main


@pytest.fixture()
def client():
    with main.app.test_client() as client:
        yield client


@pytest.mark.parametrize(
    'price,amount,state,expected_discounted_total,expected_taxed_total',
    (
        ('100.0', 100, 'UT', '9000.00', '9616.50'),
        ('100.0', 100000, 'TX', '8500000.00', '9031250.00'),
        ('123.46', 1, 'CA', '123.46', '133.65'),
        ('0', 0, 'NV', '0.00', '0.00'),
    )
)
def test_ok(
        client: flask.testing.FlaskClient,
        price: str, amount: int, state: str,
        expected_discounted_total: str, expected_taxed_total: str
):
    rv = client.post(
        '/v1/calc-cost',
        json={
            'price': price,
            'amount': amount,
            'state': state,
        }
    )
    assert rv.status_code == 200
    discounted_total = rv.json['discounted_total']
    taxed_total = rv.json['taxed_total']
    assert discounted_total == expected_discounted_total
    assert taxed_total == expected_taxed_total


@pytest.mark.parametrize(
    'price,amount,state',
    (
        ('qwe', 1, 'UT'),
        ('1', 'qwe', 'AL'),
        ('1', 1, 'qwe'),
        ('-1', 1, 'TX'),
        ('1', -1, 'NV'),
    )
)
def test_bad_request(
        client: flask.testing.FlaskClient,
        price: str, amount: int, state: str,
):

    rv = client.post(
        '/v1/calc-cost',
        json={
            'price': price,
            'amount': amount,
            'state': state,
        }
    )
    assert rv.status_code == 400
    assert rv.json['code'] == 'BAD_REQUEST'
